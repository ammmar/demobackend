package com.tn.isaam.demoExample.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtudiantDao extends JpaRepository<Etudiant, Integer> {
  //  Etudiant deleteById(int id);
}

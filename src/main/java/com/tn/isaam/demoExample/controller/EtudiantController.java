package com.tn.isaam.demoExample.controller;

import com.tn.isaam.demoExample.model.ApiResponse;
import com.tn.isaam.demoExample.model.Etudiant;
import com.tn.isaam.demoExample.model.EtudiantDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping(value ="/users")
public class EtudiantController {

    @Autowired
    private EtudiantDao etudiantdao;

    @GetMapping
    public ApiResponse<List<Etudiant>> AfficherEtudiant(){
        return new ApiResponse<>(HttpStatus.OK.value(), "User list fetched successfully.",etudiantdao.findAll());


          }


    @GetMapping("/{id}")
    public ApiResponse<Etudiant> AffEtudiant(@PathVariable int id) {
        return new ApiResponse<>(HttpStatus.OK.value(), "User fetched successfully.",etudiantdao.findById(id));

    }

    /*@GetMapping(value="/etudiant/{id}")
    public List<Etudiant> AffEtudiantClasse(@PathVariable String nom) {
        return etudiantdao.ChercherEtudant(nom);
    }*/

    @PostMapping(value="/addetudiant")
    public Etudiant AjouterEtudiant(@RequestBody Etudiant etudiant) {
        return etudiantdao.save(etudiant);
    }

   @DeleteMapping(value="/deletudiant/{id}")
    public String SuppEtudiant(@PathVariable int id) {
      etudiantdao.deleteById(id);

      return "success delete ";
    }

}
